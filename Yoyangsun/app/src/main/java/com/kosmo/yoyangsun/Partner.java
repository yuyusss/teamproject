package com.kosmo.yoyangsun;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class Partner extends AppCompatActivity {
    ProgressDialog dialog;
    SharedPreferences.Editor editor;

    int buttonResId;
    String user_id;

    TextView textView;
    Spinner spinner;
    String[] items={"계정", "시설정보 오류", "개선사항", "버그신고", "기타"};
    EditText contents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partner);

        SharedPreferences pref = getSharedPreferences("login", Activity.MODE_PRIVATE);
        editor = pref.edit();
        user_id = pref.getString("id","");

        textView = (TextView)findViewById(R.id.textView);
        contents = findViewById(R.id.partner_contents);

        Button partner_send = findViewById(R.id.partner_send);

        partner_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonResId = R.id.question_send;

                new AsyncHttpRequest().execute(
                        "http://192.168.0.61:8080/Yoyangsun/HttpAndroid/partner.jsp",
                        "user_id="+user_id,
                        "contents="+contents.getText().toString());
            }
        });
        dialog = new ProgressDialog(this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setIcon(android.R.drawable.ic_dialog_alert);
        dialog.setTitle("처리중");
        dialog.setMessage("서버로부터 응답을 기다리고 있습니다.");
    }

    class AsyncHttpRequest extends AsyncTask<String, Void, String> {

        //execute()를 호출할떄 전달한 3개의 파라미터를 가변인자를 통해 전달받는다. 함수 내부에서는 배열로 사용한다.

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //서버로 요청하는 시점에 프로그레스 대화창을 띄워준다.
            if(!dialog.isShowing()){
                dialog.show();
            }
        }


        //execute()가 호출되면 자동으로 호출되는 메소드(실제동작을처리)
        @Override
        protected String doInBackground(String... params) {

            //파라미터확인용
            for(String s : params){
                Log.i("AsyncTask Class", "파라미터:"+s);
            }

            //서버의 응답데이터를 저장할 변수(디버깅용)
            StringBuffer sBuffer = new StringBuffer();

            try{
                URL url = new URL(params[0]);
                //위 참조변수로  URL(웹주소)연결
                HttpURLConnection connection = (HttpURLConnection)url.openConnection();

                //전송방식은 POST로 설정한다.(디폴트트 GET방식)
                connection.setRequestMethod("POST");
                //OutputStream으로 파라미터를 전달하겠다는 설정
                connection.setDoOutput(true);

                /*
                요청 파라미터를 OutputStream으로 조립후 전달한다.
                - 파라미터는 쿼리스트링 형태로 지정한다.
                - 한국어를 전송하는 경우에는 URLEncode를 해야한다.
                 */
                OutputStream out = connection.getOutputStream();
                for(int i=1; i<params.length; i++){
                    out.write(params[i].getBytes());
                    out.write("&".getBytes());
                }
                /*out.write(params[1].getBytes());
                out.write("&".getBytes());
                out.write(params[2].getBytes());
                out.write("&".getBytes());
                out.write(params[3].getBytes());
                out.write("&".getBytes());
                out.write(params[4].getBytes());*/
                out.flush();
                out.close();

                /*
                getResponseCode()를 호출하면 서버로 요청이 전달된다.
                 */
                if(connection.getResponseCode() == HttpURLConnection.HTTP_OK){
                    //서버로부터 응답이 온 경우..
                    //응답데이터를 StringBuffer변수에 저장한다.
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
                    String responseData;
                    while((responseData=reader.readLine())!=null){
                        sBuffer.append(responseData+"\n\r");
                    }
                    reader.close();
                } else{
                    //응답이 없는경우
                    Log.i("AsyncTask Class", "HTTP_OK 안됨");
                }
            } catch(Exception e){
                e.printStackTrace();
            }
            // {"isLogin":1,"memberInfo":{"pass":"1234","name":"테스터1","id":"test1"}}

            if(buttonResId==R.id.question_send){
                try{
                    JSONObject jsonObject = new JSONObject(sBuffer.toString());
                    sBuffer.setLength(0);

                    sBuffer.append(jsonObject.getString("isSend")+",");
                    sBuffer.append(jsonObject.getString("msg"));
                    //sBuffer 초기화

                } catch(Exception e){
                    e.printStackTrace();
                }
            }

            return sBuffer.toString();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            //진행 대화창 닫기
            dialog.dismiss();

            //서버의 응답데이터 파싱 후 텍스트뷰에 출력
            String[] str = s.split(",");
            Toast.makeText(getApplicationContext(), str[1], Toast.LENGTH_LONG).show();
            if(str[0].equals("1")){
                finish();
            }
            //Log.i("result", s);
        }
    }
}
