package com.kosmo.yoyangsun;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import java.io.Serializable;

public class Profile extends Fragment implements View.OnClickListener {
    Context context;
    View section1, section2, section3;
    View header1, header2, header3;

    TableLayout tableLayout1, tableLayout2;
    LinearLayout hiddenLayout1, hiddenLayout2;
    TextView profile_title;

    SharedPreferences.Editor editor;
    SharedPreferences pref;
    String id, name;

    public Profile(){}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.profile_layout, container, false);

        section1 = view.findViewById(R.id.section1);
        section2 = view.findViewById(R.id.section2);
        section3 = view.findViewById(R.id.section3);
        pref = getActivity().getSharedPreferences("login", Activity.MODE_PRIVATE);
        editor = pref.edit();

        id = pref.getString("id","");
        name = pref.getString("name", "");

        profile_title = view.findViewById(R.id.profile_title);

        tableLayout1 = view.findViewById(R.id.login_tableLayout1);
        hiddenLayout1 = view.findViewById(R.id.hiddenLayout1);
        hiddenLayout2 = view.findViewById(R.id.hiddenLayout2);
        //tableLayout2 = view.findViewById(R.id.login_tableLayout2);
        //tableLayout2.setVisibility(View.GONE);
        if(!id.equals("")){
            //로그인 된 상태
            tableLayout1.setVisibility(View.GONE);
            hiddenLayout1.setVisibility(View.VISIBLE);
            hiddenLayout2.setVisibility(View.VISIBLE);

            //tableLayout2.setVisibility(View.VISIBLE);
            profile_title.setText(name+"\n("+id+")");
            float px = 80f;
            profile_title.setTextSize(px / view.getResources().getDisplayMetrics().density);
        } else{
            tableLayout1.setVisibility(View.VISIBLE);
            hiddenLayout1.setVisibility(View.GONE);
            hiddenLayout2.setVisibility(View.GONE);

            //tableLayout2.setVisibility(View.INVISIBLE);
            profile_title.setText("간편한 회원가입 또는 로그인 후, 요양선을 편리하게 이용해 보세요.");

            float px = 40f;
            profile_title.setTextSize(px / view.getResources().getDisplayMetrics().density);
        }

        Button btn_login = view.findViewById(R.id.btn_login);
        Button btn_regist = (Button)view.findViewById(R.id.btn_regist);


        TextView btn_logout = view.findViewById(R.id.btn_logout);
        TextView btn_myprofile = view.findViewById(R.id.btn_myprofile);
        TextView btn_service = view.findViewById(R.id.btn_service);
        TextView btn_partner = view.findViewById(R.id.btn_partner);
        TextView btn_mypatient = view.findViewById(R.id.btn_mypatient);

        btn_regist.setOnClickListener(this);
        btn_login.setOnClickListener(this);
        btn_logout.setOnClickListener(this);
        btn_myprofile.setOnClickListener(this);
        btn_service.setOnClickListener(this);
        btn_partner.setOnClickListener(this);
        btn_mypatient.setOnClickListener(this);

        header1 = view.findViewById(R.id.header1);
        header1.setOnClickListener(this);

        header2 = view.findViewById(R.id.header2);
        header2.setOnClickListener(this);

        header3 = view.findViewById(R.id.header3);
        header3.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.btn_login:
                intent = new Intent(v.getContext(), Login.class);

                startActivityForResult(intent, 100);
                break;
            case R.id.btn_regist:
                intent = new Intent(v.getContext(), Regist.class);

                startActivity(intent);
                break;
            case R.id.btn_myprofile:
                intent = new Intent(v.getContext(), Profile_Update.class);

                startActivity(intent);
                break;
            case R.id.btn_mypatient:
                intent = new Intent(v.getContext(), Patient.class);

                startActivity(intent);
                break;
            case R.id.btn_service:
                intent = new Intent(v.getContext(), Service.class);

                startActivity(intent);
                break;
            case R.id.btn_partner:
                intent = new Intent(v.getContext(), Partner.class);

                startActivity(intent);
                break;
            case R.id.btn_logout:
                editor.putString("id", "");
                editor.putString("pwd", "");
                editor.commit();

                tableLayout1.setVisibility(View.VISIBLE);
                hiddenLayout1.setVisibility(View.GONE);
                hiddenLayout2.setVisibility(View.GONE);
                //tableLayout2.setVisibility(View.INVISIBLE);

                profile_title.setText("간편한 회원가입 또는 로그인 후, 요양선을 편리하게 이용해 보세요.");
                float px = 40f;
                profile_title.setTextSize(px / v.getResources().getDisplayMetrics().density);
                break;
            case R.id.header1:
                if (section1.getVisibility() == View.GONE)
                    section1.setVisibility(View.VISIBLE);
                else
                    section1.setVisibility(View.GONE);
                break;
            case R.id.header2:
                if (section2.getVisibility() == View.GONE)
                    section2.setVisibility(View.VISIBLE);
                else
                    section2.setVisibility(View.GONE);
                break;
            case R.id.header3:
                if (section3.getVisibility() == View.GONE)
                    section3.setVisibility(View.VISIBLE);
                else
                    section3.setVisibility(View.GONE);
                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //resultCode = 100
        if(resultCode == 100) {
            if (data == null) {
                return;
            }
            if (data.getExtras().getString("login").equals("ok")) {
                tableLayout1.setVisibility(View.GONE);
                hiddenLayout1.setVisibility(View.VISIBLE);
                hiddenLayout2.setVisibility(View.VISIBLE);
                //tableLayout2.setVisibility(View.VISIBLE);
                id = pref.getString("id", "");
                name = pref.getString("name", "");

                profile_title.setText(name + "\n(" + id + ")");
                float px = 80f;
                profile_title.setTextSize(px / getActivity().getResources().getDisplayMetrics().density);
            }
        }
    }
}