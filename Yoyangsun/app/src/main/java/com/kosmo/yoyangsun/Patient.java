package com.kosmo.yoyangsun;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class Patient extends AppCompatActivity {

    SharedPreferences.Editor editor;
    SharedPreferences pref;

    ProgressDialog dialog;
    Button btn_addPatient;
    String id;

    LinearLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient);

        pref = this.getSharedPreferences("login", Activity.MODE_PRIVATE);
        editor = pref.edit();

        id = pref.getString("id","");

        layout = findViewById(R.id.patient_list);
        btn_addPatient = findViewById(R.id.btn_addPatient);
        btn_addPatient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        dialog = new ProgressDialog(this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setIcon(android.R.drawable.ic_dialog_alert);
        dialog.setTitle("처리중");
        dialog.setMessage("서버로부터 응답을 기다리고 있습니다.");

        new AsyncHttpRequest().execute(
                "http://192.168.0.61:8080/Yoyangsun/HttpAndroid/patientList.jsp",
                "user_id="+id);
    }

    class AsyncHttpRequest extends AsyncTask<String, Void, String> {

        //execute()를 호출할떄 전달한 3개의 파라미터를 가변인자를 통해 전달받는다. 함수 내부에서는 배열로 사용한다.

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //서버로 요청하는 시점에 프로그레스 대화창을 띄워준다.
            if(!dialog.isShowing()){
                dialog.show();
            }
        }


        //execute()가 호출되면 자동으로 호출되는 메소드(실제동작을처리)
        @Override
        protected String doInBackground(String... params) {

            //파라미터확인용
            for(String s : params){
                Log.i("AsyncTask Class", "파라미터:"+s);
            }

            //서버의 응답데이터를 저장할 변수(디버깅용)
            StringBuffer sBuffer = new StringBuffer();

            try{
                URL url = new URL(params[0]);
                //위 참조변수로  URL(웹주소)연결
                HttpURLConnection connection = (HttpURLConnection)url.openConnection();

                //전송방식은 POST로 설정한다.(디폴트트 GET방식)
                connection.setRequestMethod("POST");
                //OutputStream으로 파라미터를 전달하겠다는 설정
                connection.setDoOutput(true);

                /*
                요청 파라미터를 OutputStream으로 조립후 전달한다.
                - 파라미터는 쿼리스트링 형태로 지정한다.
                - 한국어를 전송하는 경우에는 URLEncode를 해야한다.
                 */
                OutputStream out = connection.getOutputStream();
                for(int i=1; i<params.length; i++){
                    out.write(params[i].getBytes());
                    out.write("&".getBytes());
                }
                /*out.write(params[1].getBytes());
                out.write("&".getBytes());
                out.write(params[2].getBytes());
                out.write("&".getBytes());
                out.write(params[3].getBytes());
                out.write("&".getBytes());
                out.write(params[4].getBytes());*/
                out.flush();
                out.close();

                /*
                getResponseCode()를 호출하면 서버로 요청이 전달된다.
                 */
                if(connection.getResponseCode() == HttpURLConnection.HTTP_OK){
                    //서버로부터 응답이 온 경우..
                    //응답데이터를 StringBuffer변수에 저장한다.
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
                    String responseData;
                    while((responseData=reader.readLine())!=null){
                        sBuffer.append(responseData+"\n\r");
                    }
                    reader.close();
                } else{
                    //응답이 없는경우
                    Log.i("AsyncTask Class", "HTTP_OK 안됨");
                }
            } catch(Exception e){
                e.printStackTrace();
            }
            // {"isLogin":1,"memberInfo":{"pass":"1234","name":"테스터1","id":"test1"}}

            //if(buttonResId==R.id.btn_regist_submit){
            try{
                JSONObject jsonObject = new JSONObject(sBuffer.toString());
                int success = Integer.parseInt(jsonObject.getString("isList"));

                sBuffer.setLength(0);
                //sBuffer 초기화
                //Log.i("", "jsonObject: "+jsonObject.toString());
                //Log.i("", "success: "+success);
                if(success==1){
                    JSONArray jsonArray = (JSONArray)jsonObject.get("list");
                    //Log.i("", "jsonArray: "+jsonArray.toString());
                    for(int i=0; i<jsonArray.length(); i++){
                        JSONObject object = jsonArray.getJSONObject(i);

                        View view = getLayoutInflater().inflate(R.layout.patient_card, null, false);

                        TextView regidate = view.findViewById(R.id.patient_regidate);
                        TextView birthdate = view.findViewById(R.id.patient_birthdate);
                        TextView name = view.findViewById(R.id.patient_name);

                        final String pati_idx = object.get("pati_idx").toString();

                        Button btn_patient_detail = view.findViewById(R.id.btn_patient_detail);
                        btn_patient_detail.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(v.getContext(), Patient_Detail.class);
                                intent.putExtra("pati_idx", pati_idx);

                                startActivity(intent);
                                //object.get("pati_idx");
                            }
                        });
                        regidate.setText(object.get("pati_regidate").toString());
                        birthdate.setText(object.get("pati_name").toString());
                        name.setText(object.get("pati_birthdate").toString());

                        Log.i("", "doInBackground: "+object.toString());

                        layout.addView(view);
                    }
                } else{
                    //Toast.makeText(getApplicationContext(), "", Toast.LENGTH_LONG).show();
                }
            } catch(Exception e){
                e.printStackTrace();
            }
           //}

            return sBuffer.toString();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            //진행 대화창 닫기
            dialog.dismiss();

            //서버의 응답데이터 파싱 후 텍스트뷰에 출력
            /*String[] str = s.split(",");
            Toast.makeText(getApplicationContext(), str[1], Toast.LENGTH_LONG).show();
            if(str[0].equals("1")){
                finish();
            }*/
            //Log.i("result", s);
        }
    }


}
