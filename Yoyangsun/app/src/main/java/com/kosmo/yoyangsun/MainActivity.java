package com.kosmo.yoyangsun;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.io.Serializable;

public class MainActivity extends AppCompatActivity {

    Animation translateAnimUp, translateAnimDown;
    LinearLayout page_type, page_service;
    Button button_type, button_service;
    Button button_type_close, button_service_close;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        /*view = LayoutInflater.from(this).inflate(R.layout.activity_main, null, false);
        ListView listView = new ListView(this);
        ListView.LayoutParams lp = ListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        view.setLayoutParams(lp);*/

        /*
        TabHost를 이용하여 탭메뉴 구성하기
        */
        //1.FragmentTabHost 위젯 가져오기
        FragmentTabHost fth =
                (FragmentTabHost)findViewById(android.R.id.tabhost);
        /*
        2.실제 텝메뉴 내용을 표시할 레이아웃을 FragmentManager에 연결하기
        위해 setup메소드를 호출한다.
        FragmentManager Fragment(탭메뉴구성화면)를 FragmentActivity에
        표시하는 역할을 한다.
        */
        fth.setup(this, getSupportFragmentManager(),
                R.id.realcontent);

        /*
        Bundle 클래스를 통한 데이터 전달
        -여러 타입의 값을 저장하는 Map계열의 클래스이다.
        -Javaㅇ서 DTO와 비슷한 개념이라 볼수 있다.
        Ex)
            보내는쪽
                Bundle b = new Bundle();
                b.putString("키","전달데이터");
            받는쪽
                Bundle b = getArguments();
                b.getString("키");
        -또 다른 용도로는 액티비티가 중단될때의 데이터를 가지고 있다가
        액티비티가 시작될떄 Bundle객체를 통해 전달받을수 있다.
        */

        //3.전달할 데이터가 있는 경우 Bundle객체 사용

        Bundle bundle = new Bundle();
        Ctx ctx = new Ctx(this);
        bundle.putSerializable("context", ctx);
        Intent intent = new Intent();
        //4.탭메뉴 추가 : 전달할 데이터는 마지막 매개변수를 사용하고
        //없으면 null을 쓰면 됨.
        fth.addTab(fth.newTabSpec("tab1").setIndicator("홈"),
                 Home.class, bundle);
        fth.addTab(fth.newTabSpec("tab2").setIndicator("시설찾기"),
                Search.class, bundle);
        fth.addTab(fth.newTabSpec("tab3").setIndicator("저장목록"),
                Save.class, bundle);//탭메뉴3은 데이터 전달됨
        fth.addTab(fth.newTabSpec("tab4").setIndicator("프로필"),
                Profile.class, bundle);
        fth.setCurrentTab(0);

/*        for(int i=0;i<fth.getTabWidget().getChildCount();i++){

            fth.getTabWidget().getChildAt(i)

                    .setBackgroundColor(Color.parseColor("#734512"));

        }*/

        page_type = (LinearLayout)findViewById(R.id.slide_type);
        page_service = (LinearLayout)findViewById(R.id.slide_service);

        translateAnimUp = AnimationUtils.loadAnimation(this, R.anim.translate_up);
        translateAnimDown = AnimationUtils.loadAnimation(this, R.anim.translate_down);
        button_type = findViewById(R.id.button_type);
        button_service = findViewById(R.id.button_service);

        button_type_close = findViewById(R.id.button_type_close);
        button_service_close = findViewById(R.id.button_service_close);

        Animation.AnimationListener animListener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };
        translateAnimUp.setAnimationListener(animListener);
        translateAnimDown.setAnimationListener(animListener);

        button_type_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page_type.setVisibility(View.INVISIBLE);
                page_type.startAnimation(translateAnimDown);
                Log.i("button_type_close", "onClick");
            }
        });

        button_service_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page_service.setVisibility(View.INVISIBLE);
                page_service.startAnimation(translateAnimDown);
                Log.i("button_service_close", "onClick");
            }
        });

    }

    public void showPageType(){

        page_type.setVisibility(View.VISIBLE);
        page_type.startAnimation(translateAnimUp);

        page_service.setVisibility(View.INVISIBLE);

    }
    public void showPageService(){
        page_service.setVisibility(View.VISIBLE);
        page_service.startAnimation(translateAnimUp);

        page_type.setVisibility(View.INVISIBLE);
        Log.i("button_service", "onClick");

    }

    static class Ctx implements Serializable{

        public transient Context context;

        public Ctx(Context c){
            context = c;
        }

    }


}
