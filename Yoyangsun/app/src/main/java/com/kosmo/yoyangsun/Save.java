package com.kosmo.yoyangsun;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;

public class Save extends Fragment implements View.OnClickListener {
    Context context;

    Button btn_login, btn_regist;
    LinearLayout linearLayout1, linearLayout2;
    SharedPreferences.Editor editor;

    public Save(){}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.save_layout, container, false);
        Bundle bundle = getArguments();
        context = ( (MainActivity.Ctx)bundle.getSerializable("context") ).context;

        btn_login = view.findViewById(R.id.btn_login);
        btn_regist = view.findViewById(R.id.btn_regist);

        btn_login.setOnClickListener(this);
        btn_regist.setOnClickListener(this);

        linearLayout1 = view.findViewById(R.id.linearLayout1);
        linearLayout2 = view.findViewById(R.id.linearLayout2);

        SharedPreferences pref = getActivity().getSharedPreferences("login", Activity.MODE_PRIVATE);
        editor = pref.edit();

        String id = pref.getString("id","");

        if(!id.equals("")){
            linearLayout1.setVisibility(View.INVISIBLE);
            linearLayout2.setVisibility(View.VISIBLE);
        } else{
            linearLayout1.setVisibility(View.VISIBLE);
            linearLayout2.setVisibility(View.INVISIBLE);
        }

        return view;
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.btn_login:
                intent = new Intent(v.getContext(), Login.class);

                startActivityForResult(intent, 100);
                break;
            case R.id.btn_regist:
                intent = new Intent(v.getContext(), Regist.class);

                startActivity(intent);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //resultCode = 100
        if(data==null){
            return;
        }
        if(data.getExtras().getString("login").equals("ok")){
            linearLayout1.setVisibility(View.INVISIBLE);
            linearLayout2.setVisibility(View.VISIBLE);
        }
    }
}
